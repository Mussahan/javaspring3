package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.BookGenres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookGenresRepository extends JpaRepository<BookGenres,String>
{


}
