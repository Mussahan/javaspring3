package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.BookPublisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookPublisherRepository extends JpaRepository<BookPublisher,String>
{

}
