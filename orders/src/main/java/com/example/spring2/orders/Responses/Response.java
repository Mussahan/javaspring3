package com.example.spring2.orders.Responses;


import lombok.Data;

@Data
public class Response {

    private String id;
    private String book_name;
    private String book_author;
    public String book_description;

    public Response() {
    }

    public Response(String id, String book_name, String book_author, String book_description) {
        this.id = id;
        this.book_name = book_name;
        this.book_author = book_author;
        this.book_description = book_description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getBook_name() {
        return book_name;
    }

    public void setBook_name(String book_name) {
        this.book_name = book_name;
    }

    public String getBook_author() {
        return book_author;
    }

    public void setBook_author(String book_author) {
        this.book_author = book_author;
    }

    public String getBook_description() {
        return book_description;
    }

    public void setBook_description(String book_description) {
        this.book_description = book_description;
    }
}
