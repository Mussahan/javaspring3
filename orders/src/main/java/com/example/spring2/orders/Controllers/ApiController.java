package com.example.spring2.orders.Controllers;


import com.example.spring2.orders.Responses.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/")
@CrossOrigin(origins = "*")
public class ApiController {


    @Autowired
    RestTemplate restTemplate;

    @PostMapping(value = "/values")
    public String values(){
        return "values";
    }

    @GetMapping(value = "/base_orders")
    public List<Response> base_orders(Response response) {

        List<Response> responses = new ArrayList<>();
        Response response1 = new Response();
        response1.setId("1");
        response1.setBook_author("Dima");
        response1.setBook_description("OT UMA");
        response1.setBook_name("GORE");
        responses.add(response1);

        System.out.println(response1);

        return responses;
    }
}
