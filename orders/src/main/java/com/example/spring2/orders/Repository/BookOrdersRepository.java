package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.BookOrders;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookOrdersRepository extends JpaRepository<BookOrders,String>
{

}
