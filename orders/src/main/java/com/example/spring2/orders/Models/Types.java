package com.example.spring2.orders.Models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="types")
@Getter
@Setter
public class Types {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String created_at;
    private String updated_at;
    private String name;
    private String description;


    public Types(){}
    public Types(String created_at, String updated_at, String name, String description) {
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.name = name;
        this.description = description;
    }

}
