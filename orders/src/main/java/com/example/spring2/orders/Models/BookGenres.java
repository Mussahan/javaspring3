package com.example.spring2.orders.Models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "book_genres")
@Getter
@Setter
public class BookGenres {


    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String created_at;
    private String updated_at;
    private String book_id;
    private String genre_id;

    public BookGenres(){}
    public BookGenres(String id, String created_at, String updated_at, String book_id, String genre_id) {
        this.id = id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.book_id = book_id;
        this.genre_id = genre_id;
    }
}
