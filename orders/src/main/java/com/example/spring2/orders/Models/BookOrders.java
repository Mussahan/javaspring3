package com.example.spring2.orders.Models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity(name = "book_orders")
public class BookOrders {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String created_at;
    private String updated_at;
    private String user_id;
    private String book_id;
    private String status;

    public BookOrders(){}
    public BookOrders(String created_at, String updated_at, String user_id, String book_id, String status) {
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.user_id = user_id;
        this.book_id = book_id;
        this.status = status;
    }
}