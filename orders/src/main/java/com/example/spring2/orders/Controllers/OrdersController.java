package com.example.spring2.orders.Controllers;


import com.example.spring2.orders.Models.BookOrders;
import com.example.spring2.orders.Repository.BookOrdersRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/")

public class OrdersController
{

    private BookOrdersRepository bookOrdersRepository;

    public OrdersController(BookOrdersRepository bookOrdersRepository) {
        this.bookOrdersRepository = bookOrdersRepository;
    }

//    @HystrixCommand(fallbackMethod = "fallback", commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000")
//    })
    @PostMapping(value = "/get-orders")
    public List<BookOrders> GetOrders()    {return bookOrdersRepository.findAll();}

    @RequestMapping(path = "/orders/add", method = RequestMethod.POST)
    public List<BookOrders> getAuthorById(@RequestBody BookOrders bookOrders) {

        bookOrdersRepository.save(bookOrders);

        return bookOrdersRepository.findAll();
    }

//    public Map<String, String> fallback()
//    {
//        Map<String, String> fallback = new HashMap<String, String>();
//        fallback.put("error","fallback gone");
//        return fallback;
//    }
}
