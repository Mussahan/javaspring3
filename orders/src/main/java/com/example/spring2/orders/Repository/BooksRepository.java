package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.Books;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BooksRepository extends JpaRepository<Books,String> {
}
