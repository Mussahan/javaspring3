package com.example.spring2.orders.Models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity

public class BookTypes {

    public BookTypes() {
    }

    public BookTypes(String created_at, String updated_at, String book_id, String type_id, Types types) {
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.book_id = book_id;
        this.type_id = type_id;
        this.types = types;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String created_at;
    private String updated_at;
    private String book_id;

    @Column(name = "type_id")
    private String type_id;

    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "type_id", referencedColumnName = "id", insertable = false, updatable = false)
    private Types types;

    public Types getTypes() {return types;}
    public void setTypes(Types types) {this.types = types; }

}
