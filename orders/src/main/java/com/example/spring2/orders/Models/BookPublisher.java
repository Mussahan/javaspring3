package com.example.spring2.orders.Models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Getter
@Setter
@Entity(name = "book_publisher")

public class BookPublisher {

    public BookPublisher() {}
    public BookPublisher(String id, String created_at, String updated_at, String book_id, String publisher_id) {
        this.id = id;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.book_id = book_id;
        this.publisher_id = publisher_id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String created_at;
    private String updated_at;
    private String book_id;
    private String publisher_id;
}