package com.example.spring2.orders.Models;


import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name = "publisher")
@Getter
@Setter
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private String id;
    private String created_at;
    private String updated_at;
    private String name;
    private String address;
    private String status;

    public Publisher() {}

    public Publisher(String created_at, String updated_at, String name, String address, String status) {
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.name = name;
        this.address = address;
        this.status = status;
    }
}
