package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.Types;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TypesRepository extends JpaRepository<Types,String>
{
//    @Query("select name,description from BookTypes bt left join types t on t.id = bt.type_id")

//    @Query("SELECT b from Books b left join b.genreList where b.bookName like %?1%")
//    Page<Books> findByBookName(String bookName, Pageable pageable );

}
