package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.Publisher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface PublisherRepository extends JpaRepository<Publisher,String>{
}
