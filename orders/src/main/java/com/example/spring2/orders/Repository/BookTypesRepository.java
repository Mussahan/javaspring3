package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.BookTypes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BookTypesRepository extends JpaRepository<BookTypes,String> {
//    List<BookTypes> findAllByBookId(String book_id);

//    where bt.book_id like %?1%
    @Query("select bt,t from BookTypes bt left join bt.types t where bt.book_id like %?1%")
    List<BookTypes> findAllByBookId(String book_id);
}