package com.example.spring2.orders.Controllers;


import com.example.spring2.orders.Models.Author;
import com.example.spring2.orders.Models.BookTypes;
import com.example.spring2.orders.Models.Books;
import com.example.spring2.orders.Repository.AuthorRepository;
import com.example.spring2.orders.Repository.BookTypesRepository;
import com.example.spring2.orders.Repository.BooksRepository;
import com.example.spring2.orders.Repository.TypesRepository;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")

public class BooksController {

    private BooksRepository booksRepository;
    private AuthorRepository authorRepository;
    private TypesRepository typesRepository;
    private BookTypesRepository bookTypesRepository;


    public BooksController(BooksRepository booksRepository, AuthorRepository authorRepository, TypesRepository typesRepository, BookTypesRepository bookTypesRepository) {
        this.booksRepository = booksRepository;
        this.authorRepository = authorRepository;
        this.typesRepository = typesRepository;
        this.bookTypesRepository = bookTypesRepository;
    }

    @PostMapping("/books")
    public List<Books> getBooks() {
        return booksRepository.findAll();
    }

    @PostMapping("/book/{id}")
    public Optional<Books> getExactBook(@PathVariable("id") String id) {
        return booksRepository.findById(id);
    }


    @RequestMapping(path = "/author/{id}", method = RequestMethod.GET)
    public List<Author> getAuthorById(@PathVariable String id) {
        return authorRepository.findAllById(Collections.singleton(id));
    }

    @RequestMapping(path = "/book_types/{book_id}", method = RequestMethod.GET)
    public List<BookTypes> getTypesByBookId(@PathVariable String book_id)
    {
        return bookTypesRepository.findAllByBookId(book_id);
    }

//    @RequestMapping(path = "/types/{type_id}", method = RequestMethod.GET)
//    public List<Types> getTypesId(@PathVariable String type_id) {
//        return typesRepository.findAllById(type_id);
//    }
}