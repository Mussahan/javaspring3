package com.example.spring2.orders.Repository;

import com.example.spring2.orders.Models.Author;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AuthorRepository extends JpaRepository<Author,String>
{

}