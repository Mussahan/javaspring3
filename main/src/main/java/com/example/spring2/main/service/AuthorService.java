package com.example.spring2.main.service;

import com.example.spring2.main.Models.Author;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class AuthorService {

    @Autowired
    RestTemplate restTemplate;

    public List<Author> getAuthor(String id) {

        String apiCredentials = "info-client:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<Author>> response = restTemplate.exchange(
                "http://localhost:8082/author/"+id,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<Author>>() {
                });

        return response.getBody();
    }
}