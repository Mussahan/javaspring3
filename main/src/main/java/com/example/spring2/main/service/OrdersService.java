package com.example.spring2.main.service;


import com.example.spring2.main.Models.BookOrders;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Service
public class OrdersService {

    @Autowired
    RestTemplate restTemplate;

    public List<BookOrders> getBookCatalog() {
        String apiCredentials = "info-client:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
        System.out.println(base64Credentials);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        System.out.println(headers);
        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<BookOrders>> response = restTemplate.exchange(
                "http://localhost:8082/get-orders",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<List<BookOrders>>() {
                });

        return response.getBody();
    }
}
