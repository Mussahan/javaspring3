package com.example.spring2.main.Controllers;

import com.example.spring2.main.Models.Author;
import com.example.spring2.main.Models.BookOrders;
import com.example.spring2.main.Models.BookTypes;
import com.example.spring2.main.Models.Books;
import com.example.spring2.main.service.AuthorService;
import com.example.spring2.main.service.BooksService;
import com.example.spring2.main.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/main")

public class ApiController {
    @Autowired
    private OrdersService ordersService;

    @Autowired
    private BooksService booksService;

    @Autowired
    private AuthorService authorService;

    @Autowired
    private RestTemplate restTemplate;

    @PostMapping("/")
    public String test() {
        return "test_ords";
    }

    @PostMapping("/ratings")
    public List<BookOrders> ratings() {
        return ordersService.getBookCatalog();
    }

    @PostMapping("/aa")
    public List<BookOrders> ratingsa()
    {
        String book_id = "99";
        String user_id = "99";

        return booksService.saveBookOrder(book_id,user_id);
    }


    @GetMapping("/books")
//    @JsonFormat(with = JsonFormat.Feature.ACCEPT_SINGLE_VALUE_AS_ARRAY)
    public List<Books> books() {

        List<Books> averallBooks = new ArrayList<>();
        List<Books> books = booksService.getBooks();

        for (Books book : books) {

            List<Author> authors = authorService.getAuthor(book.getAuthor_id());
            String name = null;
            String info = null;

            List<BookTypes> bookTypes = booksService.getTypesBook(book.getId());
            List<String> types = new ArrayList<>();

            for (BookTypes booktypes : bookTypes) {
                types.add(booktypes.getTypes().getName());
            }

            for (Author author : authors) {
                name = author.getName();
                info = author.getInformation();
            }

            averallBooks.add(new Books(
                    book.getId(),
                    book.getCreated_at(),
                    book.getUpdated_at(),
                    book.getName(),
                    book.getAuthor_id(),
                    book.getInformation(),
                    book.getStatus(),
                    name,
                    info,
                    types
            ));
        }

        return averallBooks;
    }
}
