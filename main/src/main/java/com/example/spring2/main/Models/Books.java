package com.example.spring2.main.Models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Books {
    private String id;
    private String created_at;
    private String updated_at;
    private String name;
    private String author_id;
    private String information;
    private String status;

    private String author_name;
    private String author_info;

    private List<String> types;


//    private List<String> types;

}
