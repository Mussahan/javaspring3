package com.example.spring2.main.Models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class Types {

    private String id;
    private String created_at;
    private String updated_at;
    private String name;
    private String description;

}
