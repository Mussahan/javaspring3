package com.example.spring2.main.service;


import com.example.spring2.main.Models.BookOrders;
import com.example.spring2.main.Models.BookTypes;
import com.example.spring2.main.Models.Books;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
public class BooksService {

    @Autowired
    RestTemplate restTemplate;

    public List<Books> getBooks()
    {
        String apiCredentials = "info-client:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);

        HttpEntity<String> entity = new HttpEntity<>(headers);

        ResponseEntity<List<Books>> response = restTemplate.exchange(
                "http://localhost:8082/books",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<List<Books>>() {
                });

        return response.getBody();
    }


    public List<BookTypes> getTypesBook(String id) {

        String apiCredentials = "info-client:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));
        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", "Basic " + base64Credentials);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        ResponseEntity<List<BookTypes>> response = restTemplate.exchange(
                "http://localhost:8082/book_types/" + id,
                HttpMethod.GET,
                entity,
                new ParameterizedTypeReference<List<BookTypes>>() {
                });

        return response.getBody();
    }

    public List<BookOrders> saveBookOrder(String book_id,String user_id)
    {
        String apiCredentials = "info-client:p@ssword";
        String base64Credentials = new String(Base64.encodeBase64(apiCredentials.getBytes()));

        HttpHeaders requestHeaders = new HttpHeaders();
        requestHeaders.setContentType(MediaType.APPLICATION_JSON);
//        requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
        requestHeaders.add("Authorization", "Basic " + base64Credentials);

        BookOrders bookOrders = new BookOrders();
        bookOrders.setCreated_at(String.valueOf(Instant.now().toEpochMilli()));
        bookOrders.setUpdated_at(String.valueOf(Instant.now().toEpochMilli()));
        bookOrders.setUser_id(user_id);
        bookOrders.setBook_id(book_id);
        bookOrders.setStatus("1");

        HttpEntity<BookOrders> entity = new HttpEntity<>(bookOrders,requestHeaders);
        System.out.println(book_id+" "+user_id);

        ResponseEntity<ArrayList<BookOrders>> response = restTemplate.exchange(
                "http://localhost:8082/orders/add",
                HttpMethod.POST,
                entity,
                new ParameterizedTypeReference<ArrayList<BookOrders>>() {
                });

        return response.getBody();
    }

}
