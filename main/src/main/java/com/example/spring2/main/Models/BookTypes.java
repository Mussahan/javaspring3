package com.example.spring2.main.Models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor

public class BookTypes {

    private String id;
    private String created_at;
    private String updated_at;
    private String book_id;
    private String type_id;
    private Types types;

}
