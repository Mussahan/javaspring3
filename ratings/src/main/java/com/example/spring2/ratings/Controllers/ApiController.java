package com.example.spring2.ratings.Controllers;


import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/")
public class ApiController {
    @PostMapping("/base_ratings")
    public String base_ratings() {
        return "base_ratings";
    }

    @PostMapping("/telep")
    public String teleport() {
        return "telep";
    }

}